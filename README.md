# GIFs
## Enunciado
1. En esta carpeta tienes una pequeña web con un formulario, un botón y un par de mensajes de error (inicialmente ocultos).
2. Crea un archivo package.json e instala un servidor como dependencia local de desarrollo. Levanta el servidor y comprueba que la web se ve correctamente.
3. Tienes que programar la web para que al darle al botón "Buscar" lance una petición a la API de Giphy y devuelva un GIF aleatorio basado en la búsqueda. Para ello:
4. En el listener del click del botón "Buscar", debajo del comentario `// Realizar la petición a la API`, tendrás que lanzar una petición fetch a la API. Tienes la URL en la constante `urlApiBusqueda`
5. Cuando llegue la respuesta del servidor pueden darse tres casos: a) Que la petición haya sido correcta y los datos lleguen bien; b) Que la petición haya sido correcta pero se devuelva un 404; c) Que la petición devuelva un error general (por ejemplo, que el dominio esté mal o la API esté caída). Vamos a implementar el código para cada uno:
 a) Si todo es correcto, obtendremos un objeto JSON. Dentro de ese objeto, tendremos la URL de la imagen en data.image_url (inspecciona la respuesta con la pestaña Network de las DevTools). Llama a la función `renderizaGif()` pasándole dicha URL.
 b) Si la API nos da una respuesta pero es un 404, tendremos que llamar a la función `muestraError404()`. Para comprobar si funciona, busca la constante `endpointApi` y modifica su valor. Esto hará que la API responda con un 404.
 c) Si la conexión no se realiza correctamente, tendremos que llamar a la función `muestraErrorGeneral()`. Para comprobar si funciona, busca la constante `urlStartApi` y modifica su dominio a uno inexistente. Esto hará que la conexión falle.

