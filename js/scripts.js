const form = document.getElementsByTagName('form')[0];
const botonBuscar = document.querySelector('.buscar');
const inputBuscar = document.querySelector('.busqueda');
const imagenGif = document.querySelector('.imagen-gif');
const notFound = document.querySelector('.not-found');
const errorMsj = document.querySelector('.error');

const apiKey = 'TUM8zjPVGI0sNGxenLytaoo8jzNMzlyr';
const urlStartApi = 'https://api.giphy.com/v1/';
const endpointApi = 'gifs/random';
const sufixApi = '&tag=';
const urlApi = urlStartApi + endpointApi + '?api_key=' + apiKey + sufixApi;

botonBuscar.addEventListener('click', () => {
  if (inputBuscar.value !== '') {
    ocultaError404();
    ocultaErrorGeneral();
    const urlApiBusqueda = urlApi + inputBuscar.value;
    // Realizar la petición a la API

  }
});

function renderizaGif(gifUrl) {
  imagenGif.src = gifUrl;
}

function muestraError404() {
  notFound.classList.remove('off');
}

function ocultaError404() {
  notFound.classList.add('off');
}

function muestraErrorGeneral() {
  errorMsj.classList.remove('off');
}

function ocultaErrorGeneral() {
  errorMsj.classList.add('off');
}

form.addEventListener('submit', e => e.preventDefault());
